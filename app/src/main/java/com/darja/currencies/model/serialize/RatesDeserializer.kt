package com.darja.currencies.model.serialize

import com.darja.currencies.model.CurrencyRate
import com.darja.currencies.model.CurrencyRatesList
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

class RatesDeserializer: JsonDeserializer<CurrencyRatesList?> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): CurrencyRatesList? {
        if (json != null) {
            val rootObj = json.asJsonObject ?: return null

            val ratesObj = rootObj["rates"]?.asJsonObject ?: return null
            val ratesCount = ratesObj.size()
            val rates = CurrencyRatesList(ratesCount)

            ratesObj.keySet().forEach({
                rates.add(CurrencyRate(it, ratesObj[it].asFloat))
            })

            return rates

        } else {
            return null
        }
    }

}