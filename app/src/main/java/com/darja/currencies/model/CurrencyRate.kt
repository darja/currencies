package com.darja.currencies.model

class CurrencyRate(
    val currencyCode: String,
    val rate: Float
)