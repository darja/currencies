package com.darja.currencies.model

import java.util.*

class CurrencyRatesList(capacity: Int): ArrayList<CurrencyRate>(capacity)