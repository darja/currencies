package com.darja.currencies.activity

import android.os.Bundle
import com.darja.currencies.R
import com.darja.currencies.fragment.converter.ConverterFragment

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, ConverterFragment())
            .commit()
    }
}
