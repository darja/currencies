package com.darja.currencies.fragment.converter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.darja.currencies.R
import com.darja.currencies.model.CurrencyRate
import com.darja.currencies.util.FormatHelper
import com.darja.currencies.util.getFloatValue

internal class ConverterAdapter: RecyclerView.Adapter<ConverterAdapter.RateViewHolder>() {
    var items: List<CurrencyRate>? = null
    var baseAmount: Float? = 1f

    var itemSelectedListener: ((String, Float?) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RateViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_currency_rate, parent, false)

        return RateViewHolder(view)
    }

    override fun getItemCount() = items?.size ?: 0

    override fun onBindViewHolder(holder: RateViewHolder, position: Int) {
        val item = items!![position]
        holder.currencyCode.text = item.currencyCode

        val baseAmount = this.baseAmount
        holder.amount.text =
            if (baseAmount != null)
                FormatHelper.formatFloatAmount(item.rate * baseAmount)
            else ""

        holder.root.setOnClickListener {
            itemSelectedListener?.invoke(item.currencyCode, holder.amount.getFloatValue())
        }
    }

    internal class RateViewHolder(view: View): RecyclerView.ViewHolder(view) {
        @BindView(R.id.currency_root) lateinit var root: View
        @BindView(R.id.currency_code) lateinit var currencyCode: TextView
        @BindView(R.id.amount) lateinit var amount: TextView

        init {
            ButterKnife.bind(this, view)
        }
    }
}