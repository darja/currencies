package com.darja.currencies.fragment.converter

import android.app.Activity
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.text.method.DigitsKeyListener
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import butterknife.BindView
import com.darja.currencies.R
import com.darja.currencies.model.CurrencyRate
import com.darja.currencies.util.FormatHelper
import com.darja.currencies.util.getFloatValue

@Suppress("ProtectedInFinal")
internal class ConverterFragmentView {
    @BindView(R.id.list) protected lateinit var list: RecyclerView
    @BindView(R.id.root) protected lateinit var root: ViewGroup
    @BindView(R.id.progress) protected lateinit var progressBar: View
    @BindView(R.id.currency_code) protected lateinit var baseCurrencyCode: TextView
    @BindView(R.id.amount) protected lateinit var baseAmount: EditText

    private var snackbar: Snackbar? = null

    private val adapter = ConverterAdapter()

    private var amountChangedListener: ((Float?) -> Unit)? = null

    fun onActivityCreated(activity: Activity?) {
        list.layoutManager = LinearLayoutManager(activity)
        list.adapter = adapter

        baseAmount.filters = arrayOf(floatNumberFilter,
            InputFilter.LengthFilter(8))
        baseAmount.keyListener = DigitsKeyListener.getInstance("0123456789" + FormatHelper.getDecimalSeparator())
        baseAmount.addTextChangedListener(amountWatcher)
    }

    fun showRates(rates: List<CurrencyRate>) {
        adapter.items = rates
        adapter.notifyDataSetChanged()
        progressBar.visibility = View.GONE

        if (snackbar?.isShown == true) {
            snackbar?.dismiss()
        }
    }

    fun clearRates() {
        adapter.items = null
        adapter.notifyDataSetChanged()
        progressBar.visibility = View.VISIBLE
    }

    fun updateRatesForBaseAmount(baseAmount: Float?) {
        adapter.baseAmount = baseAmount
        adapter.notifyDataSetChanged()
    }

    fun showErrorMessage(messageResId: Int?) {
        if (messageResId == null) {
            snackbar?.dismiss()

        } else {
            if (snackbar == null) {
                snackbar = Snackbar
                    .make(root, messageResId, Snackbar.LENGTH_INDEFINITE)
            }
            snackbar?.setText(messageResId)
            snackbar?.show()
        }
    }

    fun onCurrencyChange(l: ((String, Float?) -> Unit)?) {
        adapter.itemSelectedListener = l
    }

    fun onAmountChange(l: ((Float?) -> Unit)?) {
        amountChangedListener = l
    }

    fun showBaseCurrency(code: String) {
        baseCurrencyCode.text = code
    }

    fun showBaseAmount(amount: Float) {
        baseAmount.removeTextChangedListener(amountWatcher)
        baseAmount.setText(FormatHelper.formatFloatAmount(amount))
        baseAmount.addTextChangedListener(amountWatcher)
    }

    private val floatNumberFilter: InputFilter = InputFilter { source, _, _, destSpan, _, _ ->
        val dest = destSpan.toString()
        if (dest.isEmpty() && source.isNotEmpty()) {
            null
        } else if (source.startsWith("0") && dest.isEmpty()) {
            ""
        } else {
            val sep = FormatHelper.getDecimalSeparator()
            if (sep != null && source.contains(sep) && dest.contains(sep)) {
                ""
            } else {
                null
            }
        }
    }

    private val amountWatcher = object: TextWatcher {
        override fun afterTextChanged(s: Editable?) { amountChangedListener?.invoke(s?.getFloatValue()) }
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    }
}