package com.darja.currencies.fragment.converter

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.os.Handler
import com.darja.currencies.R
import com.darja.currencies.api.RevolutApi
import com.darja.currencies.model.CurrencyRate
import com.darja.currencies.model.CurrencyRatesList
import com.darja.currencies.util.DPLog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import javax.inject.Inject

@Suppress("ProtectedInFinal")
class ConverterViewModel @Inject constructor(): ViewModel() {
    @Inject protected lateinit var api: RevolutApi

    companion object {
        const val UPDATE_TIMEOUT = 1000L
        const val FAILED_ATTEMPTS = 5
    }

    private var handler: Handler? = Handler()
    private var apiCalls = LinkedList<Call<*>>()

    val rates = MutableLiveData<List<CurrencyRate>>()
    val error = MutableLiveData<Int?>()
    val baseCurrency = MutableLiveData<String?>()
    val baseAmount = MutableLiveData<Float?>()

    private var failedRequestAttempts = 0

    private val updateRunnable = Runnable {
        startUpdateRates()
    }

    override fun onCleared() {
        super.onCleared()
        DPLog.w("Clear ViewModel")

        stopUpdateRates()
    }

    fun setBaseCurrency(currencyCode: String, amount: Float?) {
        DPLog.checkpoint()
        baseCurrency.value = currencyCode
        baseAmount.value = amount

        DPLog.i("Base currency: [%s], amount: [%s]", baseCurrency.value, baseAmount.value)
    }

    fun startUpdateRates() {
        if (handler == null) {
            handler = Handler()
        }

        DPLog.checkpoint()

        val baseCurrency = baseCurrency.value!!
        DPLog.d("Update rates for [%s]", baseCurrency)
        val call = api.getRates(baseCurrency)
        apiCalls.add(call)
        call.enqueue(object: Callback<CurrencyRatesList> {
            override fun onFailure(call: Call<CurrencyRatesList>, t: Throwable?) {
                DPLog.e("Update rates failed", t)

                apiCalls.remove(call)

                onRequestError()
                scheduleUpdate()
            }

            override fun onResponse(call: Call<CurrencyRatesList>, response: Response<CurrencyRatesList>) {
                DPLog.d("Rates updated for [%s]", baseCurrency)
                apiCalls.remove(call)

                val updatedRates = response.body()
                if (updatedRates != null) {
                    rates.postValue(updatedRates)
                } else {
                    DPLog.w("No response")
                    onRequestError()
                }

                scheduleUpdate()
            }
        })
    }

    fun scheduleUpdate() {
        handler?.postDelayed(updateRunnable, UPDATE_TIMEOUT)
    }

    fun stopUpdateRates() {
        DPLog.checkpoint()

        apiCalls.forEach { it.cancel() }
        apiCalls.clear()

        handler?.removeCallbacks(updateRunnable)
        handler = null
    }

    fun onRequestError() {
        if (failedRequestAttempts == FAILED_ATTEMPTS) {
            if ((rates.value?.size ?: 0) == 0) {
                error.postValue(R.string.error_cannot_reach_server)
            } else {
                error.postValue(R.string.error_rates_outdated)
            }
        } else {
            failedRequestAttempts++
        }
    }
}