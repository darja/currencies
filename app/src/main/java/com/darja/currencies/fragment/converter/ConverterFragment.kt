package com.darja.currencies.fragment.converter

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import com.darja.currencies.R
import com.darja.currencies.fragment.BaseFragment
import com.darja.currencies.util.DPLog

class ConverterFragment: BaseFragment<ConverterViewModel>() {
    private lateinit var view: ConverterFragmentView

    override fun getViewModel() = ConverterViewModel::class.java

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_converter, container, false)

        view = ConverterFragmentView()
        ButterKnife.bind(view, root)

        return root
    }

    override fun onResume() {
        super.onResume()
        DPLog.checkpoint()

        viewModel.startUpdateRates()
        observeViewModel()
        observeViewEvents()
    }

    override fun onPause() {
        super.onPause()
        viewModel.stopUpdateRates()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        DPLog.checkpoint()

        viewModel.setBaseCurrency("EUR", 1f)

        view.onActivityCreated(activity)
    }

    private fun observeViewModel() {
        viewModel.rates.observe(this, Observer {
            view.showRates(it ?: emptyList())
        })

        viewModel.error.observe(this, Observer {
            view.showErrorMessage(it)
        })

        viewModel.baseCurrency.observe(this, Observer {
            if (it != null) {
                view.showBaseCurrency(it)
            }
        })

        viewModel.baseAmount.observe(this, Observer {
            if (it != null) {
                view.showBaseAmount(it)
            }
        })
    }

    private fun observeViewEvents() {
        view.onCurrencyChange { currencyCode: String, amount: Float? ->
            viewModel.stopUpdateRates()
            view.clearRates()
            viewModel.setBaseCurrency(currencyCode, amount)
            viewModel.startUpdateRates()
        }

        view.onAmountChange {
            view.updateRatesForBaseAmount(it)
        }
    }
}