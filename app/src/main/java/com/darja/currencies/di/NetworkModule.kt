package com.darja.currencies.di

import com.darja.currencies.api.RevolutApi
import com.darja.currencies.model.CurrencyRatesList
import com.darja.currencies.model.serialize.RatesDeserializer
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {
    @Provides
    @Singleton
    fun provideRevolutApi(retrofit: Retrofit) = retrofit.create(RevolutApi::class.java)

    @Provides
    @Singleton
    fun provideRetrofit() =
        Retrofit.Builder()
            .baseUrl("https://revolut.duckdns.org")
            .addConverterFactory(GsonConverterFactory.create(gson()))
            .build()

    @Provides
    @Singleton
    fun gson() = GsonBuilder()
        .registerTypeAdapter(CurrencyRatesList::class.java, RatesDeserializer())
        .create()
}