package com.darja.currencies.di

import com.darja.currencies.CurrenciesApp
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Suppress("ReplaceArrayOfWithLiteral")
@Singleton
@Component(modules = arrayOf(
    AndroidInjectionModule::class,
    AppModule::class,
    ActivityModule::class,
    NetworkModule::class
))
interface AppComponent {
    fun inject(app: CurrenciesApp)
}
