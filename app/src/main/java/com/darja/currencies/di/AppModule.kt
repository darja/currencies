package com.darja.currencies.di

import android.app.Application
import com.darja.currencies.CurrenciesApp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [(ViewModelModule::class)])
class AppModule(private val app: Application) {
    @Provides
    @Singleton
    fun providesApp() = app

    @Provides
    @Singleton
    fun providesAppContext(app: CurrenciesApp) = app.applicationContext
}