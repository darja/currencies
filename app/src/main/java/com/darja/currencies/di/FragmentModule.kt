package com.darja.currencies.di

import com.darja.currencies.fragment.converter.ConverterFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract fun converterFragment(): ConverterFragment
}