package com.darja.currencies.util

import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.NumberFormat

object FormatHelper {
    fun formatFloatAmount(amount: Float): String {
        val nf = NumberFormat.getInstance()
        nf.roundingMode = RoundingMode.HALF_DOWN
        nf.isGroupingUsed = true
        nf.maximumFractionDigits = 3
        nf.minimumFractionDigits = 1
        return nf.format(amount)
    }

    fun parseFloat(s: String): Float? {
        val parser = NumberFormat.getInstance()

        return try {
            parser.parse(s).toFloat()
        } catch (_: Exception) {
            null
        }
    }

    fun getDecimalSeparator(): Char? {
        val nf = NumberFormat.getInstance()
        if (nf is DecimalFormat) {
            val sym = nf.decimalFormatSymbols
            return sym.decimalSeparator
        }
        return null
    }
}