package com.darja.currencies.util

import android.widget.TextView

fun CharSequence.getFloatValue() = FormatHelper.parseFloat(this.toString())

fun TextView.getFloatValue() = text.getFloatValue()
